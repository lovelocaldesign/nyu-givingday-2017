<?php
   if (strlen(session_id()) < 1) {
      session_start();
   }

   require_once "db.class.php";
   $db = new DB('../config/config.ini');

   $displayonly = 1;
   $donorArr = array();
   $donorArr = $db->getDonorArr($displayonly);

$title="NYU One Day: Rise up. Meet the challenge";
$description="NYU One Day: Rise up. Meet the challenge";
$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


		
		<section class="banner challenge-message">
		<div class="container clear">
		</div>
		</section>
		<!-- /Top -->

		<!-- mini-challenge mid -->
     <section class="school-challenges">	
	
	     	<div id="school01" class="school-chart-block clear">
	     		<i class="fa fa-close btn-close---modal"></i>
	     		<!-- BEGIN CHART 01 -->
	     		<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:30%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/cas.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 01 -->
				</div>

				<div id="school02" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 02 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/gallatin.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 02 -->
				</div>

				<div id="school03" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 03 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/law.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 03 -->
				</div>
				<div id="school04" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 04 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/silver.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 04 -->
				</div>
				<div id="school05" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 05 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/stern.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 05 -->
				</div>
				<div id="school06" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 06 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/tandon.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 06 -->
				</div>
				<div id="school07" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 07 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/wagner.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 07 -->
				</div>

				<div id="school08" class="school-chart-block clear">
					<i class="fa fa-close btn-close---modal"></i>
					<!-- BEGIN CHART 08 -->
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<div class="thermometer--block">
					  <div class="thermometer thermometer--size">
					    <span class="marker marker-0"><h2><strong>0%</strong></h2></span>
					    <span class="marker marker-1"><h2><strong>25%</strong></h2></span>
					    <span class="marker marker-2"><h2><strong>50%</strong></h2></span>
					    <span class="marker marker-3"><h2><strong>75%</strong></h2></span>
					    <span class="marker marker-4"><h2><strong>100%</strong></h2></span>
					    <div id="mercury--mask" class="mercury--mask" style="left:75%">
					       <span id="numberDonors"></span>
					    </div>
					    <img class="thermometer--logo" src="img/challenges-pg/white/steinhardt.png" alt="School logo">
					  </div> 
					</div>
					<!-- nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_nyu_ -->
					<!-- END CHART 08 -->
				</div>



			<div class="container clear">
							<div class="school-list-block school-col">
								<ul>
				<!-- 					<li class="school-list-item"><a class="btn-open---modal" data-modal="#school01" href="#"><img src="img/challenges-pg/cas.png" alt=""></a></li> -->
							<!-- 		<li class="school-list-item"><a class="btn-open---modal" data-modal="#school02" href="#"><img src="img/challenges-pg/gallatin.png" alt=""></a></li> -->
						<!-- 			<li class="school-list-item"><a class="btn-open---modal" data-modal="#school03" href="#"><img src="img/challenges-pg/law.png" alt=""></a></li> -->
									<li class="school-list-item"><a class="btn-open---modal" data-modal="#school04" href="#"><img src="img/challenges-pg/silver.png" alt=""></a></li>
									<li class="school-list-item"><a class="btn-open---modal" data-modal="#school05" href="#"><img src="img/challenges-pg/stern.png" alt=""></a></li>
									<li class="school-list-item"><a class="btn-open---modal" data-modal="#school06" href="#"><img src="img/challenges-pg/tandon.png" alt=""></a></li>
									<li class="school-list-item"><a class="btn-open---modal" data-modal="#school07" href="#"><img src="img/challenges-pg/wagner.png" alt=""></a></li>
									<li class="school-list-item"><a class="btn-open---modal" data-modal="#school08" href="#"><img src="img/challenges-pg/steinhardt.png" alt=""></a></li>
								</ul>
							</div>
							<div class="school-col school-text default">
								 <div id="school00-text">
								 	<h2 class="school-msg-headline ">
								 				          <img src="img/challenge-headline-where.png" alt="Where do you stand?">
								 				            <span class="sr-text">Where do you stand?</span> 
								 				          </h2>
								 				           <p>
								 				           We’re challenging the entire NYU community to join together in support of our students’ dreams. Will your school rise up and meet the challenge? Check back here often for school specific updated results throughout the day. 
								 				          </p>
								 				          <p><strong>We are NYU. And we’re just getting started. check back.</strong></p>
								 </div> <!-- school00-text -->

								 <div id="school01-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">CAS: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school02-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">Gallatin: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school03-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">Law: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school04-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">silver: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school05-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">Stern: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school06-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">TANDON: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school07-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">Wagner: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school08-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">steinhardt: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school09-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">09: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>

								 <div id="school10-text" class="school-text-indivisual">
									<h2 class="school-msg-headline ">
									DONOR PARTICIPATION GOAL       
									<span class="">10: 100 DONORS</span> 
									</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in repre henderit in.
									</p>
								 </div>


							</div> <!-- school-text -->
					</div> <!-- containter -->

		</section>
		<!-- /Mid -->

		<!-- bot -->
     <section class="breaking-news" >
	
			 <div class="container clear" style="display: none;">
        <div class="presmessage-col presmessage-video">
            <div class="videoOuterWrap">
              <div id="videoWrap" class="videoWrap">
                <iframe width="100%" height="" src="https://www.youtube.com/embed/6DBi41reeF0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
            <div class="block-videoLabel">
              <h2 class="video-name">Andrew Hamilton <br/>
              <span class="video-title">President</span></h2>
            </div>
        </div>

        <div class="presmessage-col presmessage-text">
          <h2 class="presmessage-headline ">
          <img src="img/hp-headline-pressmessage.png" alt="Section title01">
            <span class="sr-text">Section title01</span> 
          </h2>
           <p>
           Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eu placerat lorem. Curabitur tincidunt lectus vitae felis rutrum ultrices. In interdum tempor condimentum. Nullam semper finibus odio, ac dignissim diam interdum eget. Vivamus euismod tellus eu nunc vestibulum tempus. Nullam nunc odio, sagittis eget justo vel, accumsan volutpat lorem. Nunc ac ullamcorper nisi. Suspendisse dignissim, lectus in aliquet tincidunt, ex libero accumsan est, ut elementum dui quam quis ligula.
          </p>
           <a href="givenow.php" class="btn btn-green"> <span class="btn-inner">I will support #NYUONEDAY</span> </a>
        </div>

      </div>
			
		</section>
		<!-- /bot -->

<?php require($INC_DIR. "footer.php"); ?>
