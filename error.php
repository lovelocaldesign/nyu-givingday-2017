<?php
if (strlen(session_id()) < 1) {
    session_start();
}
# parse the error message otherwise send back to the form !!!!!
#if (!isset($_REQUEST['auth_trans_ref_no'])) {
#   header("location:givenow");
#   exit();
#}

require_once "data.class.php";
$dt = new DT;

$queryArr = array();
foreach ($_REQUEST as $k => $v) {
    $queryArr[$k] = $v;
    $dt->debugLog("{$k} = {$v}");
} # foreach

$ence = isset($_REQUEST['enc']) && $_REQUEST['enc'] ? trim(strip_tags($_REQUEST["enc"])) : (isset($_SESSION['enc']) && $_SESSION['enc'] ? trim(strip_tags($_SESSION["enc"])) : '');

if (isset($ence) && $ence) {
   $_SESSION['enc'] = $ence;
}

$title="NYU One Day: Give Now";
$description="NYU One Day: Give Now";
$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="container" id="container">
        <!-- Give Now Error -->
        <section class="thankyou-msg">
			<div class="row">
				<div class="twelve columns thank-you-msg">
                   <h1 class="banner-thankyou-headline">Error</h1>
				   <p class="copy copy--center">There was an error and your payment could not be completed. Please, <a href="givenow<?php echo $ence ? "/enc={$ence}" : ""; ?>">try again</a>.</p>
                    <h3><span class="ln">This is NYU.</span> <span class="ln">This is how we do dreams.</span></h3>

                   <p class="copy copy--center">
                   There is still time for others to join us. Please share #NYUOneDay and tell your friends how you did giving on NYU One Day! #NYUOneDay #VioletPride</p>

               <ul class=" socialicon--thankyou">
                  <li><a class="sharepost" href="https://www.facebook.com/sharer/sharer.php?u=nyuoneday.org" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li><a class="sharepost" href="https://twitter.com/intent/tweet/?text=I%20joined%20%23NYUOneDay.%20Will%20you%3F&url=https%3A%2F%2Fnyuoneday.org%2F"
   target="_blank"><i class="fa fa-twitter"></i></a></li>
               </ul>
				</div>
			</div>
		</section>
		<!-- /Give Now Error -->

	</div>
<?php require($INC_DIR. "footer.php"); ?>
