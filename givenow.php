<?php
if (strlen(session_id()) < 1) {
    session_start();
}

require_once "data.class.php";
$dt = new DT;
$monthArr = $dt->getmonths();
$countryArr = $dt->getCountries();
$fundArr = $dt->getFunds();

$ence = isset($_REQUEST['enc']) && $_REQUEST['enc'] ? trim(strip_tags($_REQUEST["enc"])) : (isset($_SESSION['enc']) && $_SESSION['enc'] ? trim(strip_tags($_SESSION["enc"])) : '');

if (isset($ence) && $ence) {
   $_SESSION['enc'] = $ence;
}

require_once "db.class.php";
$db = new DB;
$userArr = $db->getUserArr($ence);

$title="NYU One Day: This is how we do dreams";
$description="NYU One Day: This is how we do dreams";
$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<!-- Banner -->

		<section class="banner clear">
      <div class="container clear">
  				<h1 class="banner-headline sr-text">Invest in Dreams</h1>
  		</div>
		</section>
		<!-- /Banner -->


	<div class="" id="container">
		<!-- Content -->
        <section class="content">
			<!--Form Begins -->
			<form class="form-givenow" id="givenowform">

			<div class="row bg-white">
				<div class="twelve columns">
					<h4 class="your-gift">Your Gift</h4>
					<p><label for="make-gift"><span class="star">*</span>I would like to make my gift to:</label>
						<select name="make-gift" id="make-gift">
						  <option value="">Please select below</option>
								<?php

								foreach ($fundArr as $category => $fndArr) {
								   if ("Category" != $category) {
								      echo "<option value=\"{$category}\">{$category}</option>" . PHP_EOL;
								   }
								} # foreach ($fundArr as $category => $fndArr)

                                ?>
						</select>
					</p>

					<p id="subcat"></p>
					<p id="campaign"></p>

					<p class="p-other-fund"><label for="other-fund"><strong>Don't see what you'd like to support?</strong> Please complete the field below and we will ensure 100% of your gift goes toward your choice.</label>
					<input name="other-fund" id="other-fund" type="text" /></p>

				</div>
			</div>
			<div class="row bg-lightpurple">
				<div class="twelve columns">
					<h4 class="your-gift">Type of Gift</h4>

					<p>What type of gift would you like to make?
						<span id="typeOfGiftQuestion" class="fa-container btn-popup">
						  <i class="fa fa-question"></i>
						</span>
						<br/ >
						<span class="star">*</span>Gift Type
					</p>

          <p class="p-radio">
          	<input id="recurringgift" name="gifttype" value="recurringgift" type="radio">
          	<label for="recurringgift">Recurring Gift</label></p>
       		<p class="p-radio p-radio-last clearfix">
       			<input id="onetimegift" name="gifttype" value="onetimegift" type="radio">
       			<label for="onetimegift">One-Time Gift</label></p>

					<p class="p-payAmount"><label for="giftamount"><span class="star">*</span>Gift Amount:</label> <input type="text" id="giftamount" name="gift" value="$" placeholder="$" /></p>

          <p class="p-payAmount" id="gfrequency" style="display: none;"><label for="giftfrequency"><span class="star">*</span>Gift Frequency:</label>
           	 <select name="giftfrequency" id="giftfrequency">
           							  <option value=""> Please select below </option>
           	  <option value="12"> monthly </option>
           	  <option value="4"> quarterly </option>
           	  <option value="1"> annually </option>
           	 </select>
           </p>

          <p class="p-payAmount" id="gduration" style="display: none;"><label for="numpayments"><span class="star">*</span>Gift Duration:</label>
            <select name="numpayments" id="numpayments">
						  <option value=""> Please select below  </option>
              <option value="1"> 1 year </option>
              <option value="2"> 2 years </option>
              <option value="3"> 3 years </option>
						</select>
					</p>
					<p><input type="checkbox" id="anonymous" name="anonymous" value="anonymous" />
					<label for="anonymous"><span class="anonymous">I would like to make this gift anonymous.</span></label></p>
				</div>
			</div>
			<div class="row">
				<div class="twelve columns">
					<h4 class="your-gift">Contact Information</h4>
					<div class="row">
						<div class="one-third column"><input type="text" name="first-name" placeholder="*First Name" value="<?php $dt->prePopulate('first-name',$userArr); ?>" /></div>
						<div class="one-third column"><input type="text" name="middle-name" placeholder="Middle Name" value="<?php $dt->prePopulate('middle-name',$userArr); ?>" /></div>
						<div class="one-third column"><input type="text" name="last-name" placeholder="*Last Name" value="<?php $dt->prePopulate('last-name',$userArr); ?>" /></div>
					</div>
					<div class="row">
						<div class="column"><input type="text" name="street-address" placeholder="*Street Address" value="<?php $dt->prePopulate('street-address',$userArr); ?>" /></div>
					</div>
					<div class="row">
						<div class="one-half column"><input type="text" name="city" placeholder="*City" value="<?php $dt->prePopulate('city',$userArr); ?>" /></div>
						<div class="one-half column">
							<select name="country" id="country">
								<option value="">*Select Country</option>
								<?php

								foreach ($countryArr as $countkey => $countArr) {
								   if ($countkey > 0) {
								      if (isset($userArr['country']) && $userArr['country'] && $countArr['iso3'] == $userArr['country']) {
   								         echo "<option value=\"{$countArr['iso3']}\" selected=\"selected\">{$countArr['country']}</option>" . PHP_EOL;
   								      } else {
								         echo "<option value=\"{$countArr['iso3']}\">{$countArr['country']}</option>" . PHP_EOL;
								         }
								   }
								} # foreach ($countryArr as $countArr)

                                ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="one-half column" id="statediv"><input type="text" name="state" placeholder="*State" value="<?php $dt->prePopulate('state',$userArr); ?>" /></div>
						<div class="one-half column" id="zipdiv"><input type="text" name="zip" placeholder="*Zip" value="<?php $dt->prePopulate('zip',$userArr); ?>" /></div>
					</div>
					<div class="row">
						<div class="one-half column">
							<input type="text" name="phone" Placeholder="*Phone" value="<?php $dt->prePopulate('phone',$userArr); ?>" />
						</div>
						<div class="one-half column">
							<input type="text" name="email" Placeholder="*Email" value="<?php $dt->prePopulate('email',$userArr); ?>" />
						</div>
					</div>
					<div class="row">
						<div class="one-half column">
							<input type="text" name="net-id" Placeholder="Net ID" value="<?php $dt->prePopulate('net-id',$userArr); ?>" />
							<p class="example">Example: ex123@nyu.edu. The ex123 is your Net ID.</p>
						</div>
					</div>
					<div class="row">
						<div class="columns">
							<input type="checkbox" name="different-billing" value="different-billing" />
							<span class="billig-different">My billing information is different than above</span>
						</div>
					</div>
				</div>
			</div>
			<div id="Show" class="row">
				<div class="twelve columns">
					<h4 class="your-gift">Billing Information</h4>
					<div class="row">
					<input type="text" name="billing-street-address" placeholder="*Street Address" />
					</div>
					<div class="row">
						<div class="one-half column"><input type="text" name="billing-city" placeholder="*City" /></div>
						<div class="one-half column">
							<select name="billing-country" id="billing-country">
								<option value="">*Select Billing Country</option>
								<?php

								foreach ($countryArr as $countkey => $countArr) {
								   if ($countkey > 0) {
								      echo "<option value=\"{$countArr['iso3']}\">{$countArr['country']}</option>" . PHP_EOL;
								   }
								} # foreach ($countryArr as $countArr)

                                ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="one-half column" id="billstate"><input type="text" name="billing-state" placeholder="*Billing State" /></div>
						<div class="one-half column" id="billzip"><input type="text" name="billing-zip" placeholder="*Billing Zip" /></div>
					</div>
				</div>
			</div>

			<div class="row">
               <p id="errmsg"></p>
			</div>

			<div class="row">
				<div class="twelve columns" id="submitbutton">
					<input type="submit" class="btn btn-purple btn-centered" value="Next" />
				</div>
			</div>
			</form>
			<!-- Form Ends -->
		</section>
		<!-- /Content -->
	</div>


<?php require($INC_DIR. "footer.php"); ?>
