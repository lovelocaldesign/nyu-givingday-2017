<?php
   if (strlen(session_id()) < 1) {
      session_start();
   }

   require_once "db.class.php";
   $db = new DB('../config/config.ini');

   $displayonly = 1;
   $donorArr = array();
   $donorArr = $db->getDonorArr($displayonly);

$title="NYU One Day: Dreams Realized. Thanks to you.";
$description="NYU One Day: Dreams Realized. Thanks to you.";
$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;

//for the donor list.
	$heightDesktop = ((count($donorArr) / 4) * 43). 'px';  
	$heightMobile = ((count($donorArr) / 2) * 37). 'px'; 

require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Banner-impact -->
		<section class="banner-impact  bg-purple">
			<div class="container" style="max-width:1080px;">
							<div class="main-slick">

								<div>
									<div class="impact-col impact-image">
										<img class="impact-image-student" src="img/impact-pg/kovach.jpg" alt="">
										<h1>
											<img src="img/impact-pg/impact-headline-img.png" alt="Dreams realized. Thanks to you.">
										</h1>
									</div>
									<div class="impact-col impact-text">
										<h2>Tanner Kovach</h2>
										<strong>Anthropology</strong>
											<p>This year Tanner declared his major: biological anthropology. He’s considering a minor in genomics to prepare for an exciting career in the field of biological anthropology. In the spring, he’s off to study abroad in London - a once in a lifetime educational and cultural opportunity he’s been dreaming of.</p>
											<img class="impact-image-quote" src="img/impact-pg/quote-kovach.png" alt="Quote from Kovach">
											<p class="sr-text">“My scholarship helps ease my parents financial load. Thank you so much for making it possible for me to pursue my dreams at NYU.”</p>
									</div>
								</div>


								<div>
									<div class="impact-col impact-image">
										<img class="impact-image-student" src="img/impact-pg/salik.jpg" alt="">
										<h1>
											<img src="img/impact-pg/impact-headline-img.png" alt="Dreams realized. Thanks to you.">
										</h1>
									</div>
									<div class="impact-col impact-text">
										<h2>Amber Salik</h2>
										<strong>Fashion Merchandising and Photography</strong>
											<p>Amber has come to love the diversity that comes with an NYU education. Meeting so many people from different backgrounds - from classmates, to professors - has opened her eyes to the uniqueness of New York City.</p>
											<img class="impact-image-quote" src="img/impact-pg/quote-salik.png" alt="Quote from Salik">
											<p class="sr-text">“I received a 1831 Fund Scholarship through Gallatin and I’m so thankful for the investment in my future. Without financial assistance, my NYU dreams wouldn’t be possible. I’m determined not to let down all those who have invested in me.” </p>
									</div>
								</div>
				
				
								<div>
									<div class="impact-col impact-image">
										<img class="impact-image-student" src="img/impact-pg/jimenez.jpg" alt="">
										<h1>
											<img src="img/impact-pg/impact-headline-img.png" alt="Dreams realized. Thanks to you.">
										</h1>
									</div>
									<div class="impact-col impact-text">
										<h2>Jasmin Jimenez</h2>
											<strong>Environmental Science</strong>
											<p>Jasmin knows NYU is exactly where she needs to be. She values the diversity that’s a hallmark of an NYU education - on campus, in the classroom, and in the surrounding city. She knows it enhances her educational experience to learn not only from classes, but also from all the extraordinary people around her.</p>
											<img class="impact-image-quote" src="img/impact-pg/quote-jimenez.png" alt="Quote from Jimenez">
											<p class="sr-text">"Had it not been for my scholarship, I’d never be able to afford my dream school. You’ve taught me that dreams do come true. NYU is exactly where I need to be. I’m forever grateful that I’m here."</p>
									</div>
								</div>
				
				
				
								<div>
									<div class="impact-col impact-image">
										<img class="impact-image-student" src="img/impact-pg/li.jpg" alt="">
										<h1>
											<img src="img/impact-pg/impact-headline-img.png" alt="Dreams realized. Thanks to you.">
										</h1>
									</div>
									<div class="impact-col impact-text">
										<h2>Lily Li</h2>
											<strong>Media, Culture and Communications</strong>
											<p>Lily has walked through every door of opportunity opened to her at NYU.  She’s pursuing a degree in Media, Culture and Communications and loves being immersed in the culture and creativity of NYC. After graduation, Lily has set her sights on becoming a creative director at a nonprofit organization.</p>
											<img class="impact-image-quote" src="img/impact-pg/quote-li.png" alt="Quote from Li">
											<p class="sr-text">"When I looked at NYU, I tried to convince myself I wasn't in love. But I was - I was so hopelessly in love! When I saw my scholarships for the first time, I finally allowed myself to dream. I can’t thank you enough."</p>
									</div>
								</div>
				
				
				
								<div>
									<div class="impact-col impact-image">
										<img class="impact-image-student" src="img/impact-pg/gibson.jpg" alt="">
										<h1>
											<img src="img/impact-pg/impact-headline-img.png" alt="Dreams realized. Thanks to you.">
										</h1>
									</div>
									<div class="impact-col impact-text">
										<h2>Matthew Gregory Gibson</h2>
											<strong>Global Citizenship</strong>
											<p>At NYU, Matthew is learning what it means to be a global citizen. He knows that the diversity of people and ideas that an NYU education offers is unrivaled. When he’s not focused on mastering Mandarin Chinese, Matthew volunteers with campus groups and enjoys the exciting cultural scene of New York City.</p>
											<img class="impact-image-quote" src="img/impact-pg/quote-gibson.png" alt="Quote from Gibson">
											<p class="sr-text">"I believe education is critical to a person's well-being. My acceptance to this school means more than I can say. I’m extremely grateful to the donors of my scholarships that have helped me pursue self-improvement through education."</p>
									</div>
								</div>
				
				
				
				
							</div>
					</div>
		</section>
		<!-- /Banner-impact -->


		<!-- All gifts -->
     <section class="givingstats bg-grey">
			<div class="container clear">
				<h2><img src="img/impact-headline-2016stats.png" alt=""></h2>
				<img class="givingstats-desktop" src="img/impact-stats-icon2016.png" alt="">
			</div>
		</section>
		<!-- /All gifts -->

	<!-- supporters  -->
   <section class="supporters bg-teal">
		<div class="container clear">
			<h2 class="supporters-headline">#NYUOneDay Donors</h2>
				<div class="suppoters--scroll">

									<ul class="suppoters-list texts clearfix" >

									<?php
									foreach ($donorArr as $donorid => $donArr) {

                    echo "<li  class=\"supporters-names\">{$donArr['dispname']}</li>" . PHP_EOL;

									} # foreach ($donorArr as $donorid => $donArr)

                                    ?>

                  </ul>
								</div>
		</div>
	</section>




<?php require($INC_DIR. "footer.php"); ?>
