	<footer>
		<div class="container">
			<div class="row">
				<div class="two-thirds column contact"> 
					<p class="contact-block01">
					Questions?  Want to make a gift by phone? <!-- 

					Don't want to wait? <a href="https://www.nyu.edu/giving/give-now/" target="_blank">Make an early gift now.</a> --></p>
					<p class="contact-block02">Contact 1-800-698-4144 (10am - 10pm EST) or <a href="mailto:thefund@nyu.edu">thefund@nyu.edu</a></p>
				</div>
				<div class="one-third column" style="text-align:right;">
				<a class="logo-footer" href="https://www.nyu.edu" target="_blank"><img src="img/common/nyu-logo-footer.png" alt=""></a>
					<ul class="socialicon">
						<li><a href="https://www.instagram.com/nyualumni/" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<li><a href="https://www.facebook.com/nyualumni/" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/NYUAlumni" target="_blank"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="js/nyu.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
	<script src="js/countdown.min.js"></script>
	<script>
		//var clock = document.getElementById("countdown-holder"), targetDate = new Date(2016, 03, 03); // March 3, 2016;
		//clock.innerHTML = countdown(targetDate).toString();

		// set the date we're counting down to
		var target_date = new Date("Mar 23, 2017").getTime();

		// variables for time units
		var hours, minutes, seconds;

		// get tag element
		var countdown =  document.getElementById("countdown-holder");
		var days_span = document.createElement("SPAN");
		days_span.className = 'days';
		countdown.appendChild(days_span);
		var hours_span = document.createElement("SPAN");
		hours_span.className = 'hours';
		countdown.appendChild(hours_span);
		var minutes_span = document.createElement("SPAN");
		minutes_span.className = 'minutes';
		countdown.appendChild(minutes_span);
		var secs_span = document.createElement("SPAN");
		secs_span.className = 'secs';
		countdown.appendChild(secs_span);

		// update the tag with id "countdown" every 1 second
		setInterval(function () {

			// find the amount of "seconds" between now and target
			var current_date = new Date().getTime();
			var seconds_left = (target_date - current_date) / 1000;

			// do some time calculations
			days = parseInt(seconds_left / 86400);
			seconds_left = seconds_left % 86400;

			hours = parseInt(seconds_left / 3600);
			seconds_left = seconds_left % 3600;

			minutes = parseInt(seconds_left / 60);
			seconds = parseInt(seconds_left % 60);

			// format countdown string + set tag value.
			if (days == 1)
			{
				days_span.innerHTML = '<span class="label">' + days + '</span>' + ' day, ';
			} else if (days > 1) {
				days_span.innerHTML = '<span class="label">' + days + '</span>' + ' days, ';
			}

			if (hours == 1)
			{
				hours_span.innerHTML = '<span class="label">' + hours + '</span>' + ' Hour, ';
			}
			else
			{
				hours_span.innerHTML = '<span class="label">' + hours + '</span>' + ' Hours, ';
			}
			if (minutes == 1)
			{
				minutes_span.innerHTML = '<span class="label">' + minutes + '</span>' + ' Minute, ';
			}
			else
			{
				minutes_span.innerHTML = '<span class="label">' + minutes + '</span>' + ' Minutes, ';
			}

			if (seconds == 1)
			{
				secs_span.innerHTML = '<span class="label">' + seconds + '</span>' + ' Second ';
			}
			else
			{
				secs_span.innerHTML = '<span class="label">' + seconds + '</span>' + ' Seconds ';
			}


			//countdown.innerHTML = days + "d, " + hours + "h, "
		   // + minutes + "m, " + seconds + "s";

		}, 1000);


		<?php if ($pageNameClass == 'givenow'):  ?>
		// only show on givenow.php
			var typeOfGiftAlert =  'A recurring gift is the easy way to give! For example, if you would like to give $10 each month, simply enter $10 in the "Gift Amount" box and select "Monthly" in the Frequency box. ';

			// alert buttons
			$( "#typeOfGiftQuestion" ).on("click", function(e) {
			  e.preventDefault();
			  alert( typeOfGiftAlert );
			  return false;
			});
		<?php endif; ?>

	</script>
<?php if ($pageNameClass == 'impact'): ?>
	<script type="text/javascript" src="js/slick.min.js"></script>
	<script>
//for the slider
	$( document ).ready(function() {
		$('.main-slick').slick({
			 dots: true,
			 arrows: true,
			 autoplay: true,
  		 autoplaySpeed: 4000
		});
	});

</script>
<? endif; ?>

<?php if ($pageNameClass == 'challenges'): ?>
<script>
// Simple universal modal popup/close buttons.
// Just use the appropiate classes (see the first 2 var)
// The open link will need an extra data-modal attr. which is the 'id '
// of the modal you want open.
$( document ).ready(function() {
	var closeBtn = $('.btn-close---modal');
	var openBtn = $('.btn-open---modal');


	function fadeSlide(e) {
		$(e).animate({
		  height: "toggle",
		  opacity: "toggle"
		});
	}


	 $( openBtn ).click(function(event) {
 			event.preventDefault(event);
      var modal = $(this).attr('data-modal');
			//jQuery('.school-chart-block.activeChart').slideToggle('fast');
			fadeSlide('.school-chart-block.activeChart');

			jQuery('.btn-open---modal.activeChart').removeClass('activeChart');
			jQuery('.school-chart-block.activeChart').removeClass('activeChart');
			//jQuery('.school-list-item.activeChart').slideToggle('slow');
			fadeSlide('.school-list-item.activeChart');

			jQuery('.school-list-item.activeChart').removeClass('activeChart');
			jQuery(modal).addClass('activeChart');
			//jQuery(modal).slideToggle('slow');
			fadeSlide(modal);
			jQuery(this).addClass('activeChart');

			jQuery(this).parent('.school-list-item').addClass('activeChart');
			//jQuery('.school-list-item.activeChart').slideToggle('slow');
			fadeSlide('.school-list-item.activeChart');
      return false;
    });
	 	$(closeBtn).click(function(event) {
 			event.preventDefault(event);
      //$(this).parent().slideToggle('fast');
      fadeSlide('.school-chart-block.activeChart');
      //jQuery('.school-list-item.activeChart').slideToggle('slow');
      fadeSlide('.school-list-item.activeChart');
     	jQuery('.btn-open---modal.activeChart').removeClass('activeChart');
			jQuery('.school-chart-block.activeChart').removeClass('activeChart');
			jQuery('.school-list-item.activeChart').removeClass('activeChart');
      return false;
    });
});

</script>
<? endif; ?>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-73364348-1','auto');ga('send','pageview');
        </script>

    </body>
</html>



