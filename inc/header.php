<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title ?></title>
        <meta name="description" content="<?php echo $description ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<meta property="og:url"                content="http://nyuoneday.org/" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="One Day. One Dream. One NYU." />
<meta property="og:description"        content="One day. Countless dreams. Join us. March 23, 2017" />
<meta property="og:image"              content="http://www.nyuoneday.org/img/common/NYUGFBpost.jpg" />
<meta property="og:image:type" 		   content="image/jpg">
<meta property="og:image:width"        content="1200" />
<meta property="og:image:height"       content="630" />
<meta property="fb:app_id"			   content="798627976947401"

        <?php
           if (isset($description) && $description) { ?>
        <meta content="<?php echo $description ?>" name="description" />
        <?php
           } ?>

        <meta content="NYU One Day, NYU Giving Day, NYU Giving, giving, NYU Scholarship, NYU" name="keywords" />

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		<script type="text/javascript" src="js/modernizr-custom.js"></script>
		<?php
		$homepage = "/index.php";
		$homepage1 = "/";
		$currentpage = $_SERVER['REQUEST_URI'];

		$pageNameClass = trim( basename($_SERVER['PHP_SELF']),".php"); //needed to get specific classes for pages
		?>
		<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7436432/714802/css/fonts.css" />
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="//cdn.jsdelivr.net/g/animatecss@3.5.1,normalize@3.0.3">

		<!-- can't use v3 yet because of teh socialwall needs 2.2.0 -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- 			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script> -->

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/main.css?v=2">



	
<?php
		if(($homepage==$currentpage) || ($homepage1==$currentpage) ) {
		echo '<!-- if statement if index.php -->
			<script type="text/javascript" src="js/jquery.social.stream.wall.1.6.js"></script>
			<script type="text/javascript" src="js/jquery.social.stream.1.5.15.min.js"></script>
			<link rel="stylesheet" type="text/css" href="css/dcsns_light.css" media="all" />
			<link rel="stylesheet" type="text/css" href="css/dcsns_wall.css" media="all" />
		<!-- / if statement if index.php -->


		<script type="text/javascript">
			$(document).ready(function($){
			$("#social-stream").dcSocialStream({
			feeds: {
				twitter: {
				id: "/2493532603,#NYUOneDay,NYUAlumni",
				thumb: true
				},
				// rss: {
				// 	id: ""
				// },
				// stumbleupon: {
				// 	id: ""
				// },
				facebook: {
				id: "113007344927,#NYUOneDay",
				image_width: 2
				},
				// google: {
				// 	id: ""
				// },
				// delicious: {
				// 	id: ""
				// },
				// vimeo: {
				// 	id: "brad"
				// },
				// youtube: {
				// 	id: ""
				// },
				// pinterest: {
				// 	id: ""
				// },
				// flickr: {
				// 	id: ""
				// },
				// lastfm: {
				// 	id: ""
				// },
				// dribbble: {
				// 	id: ""
				// },
				// deviantart: {
				// 	id: ""
				// },
				// tumblr: {
				// 	id: "",
				// 	thumb: 250
				// }
				 instagram: {
				 				id: "!575049520, #NYUOneDay",
				 				accessToken: "186786085.91dbf99.da4d8fab71544cdba8645bd0a02f07a1",
				 				redirectUrl: "http:nyuoneday.org",
				 				clientId: "4b212372c353414ca145316a4c7e0e5a",
				 				comments: 3,
				 				likes: 10
				 			}
			},
			rotate: {
			delay: 0
			},
			twitterId: "NYUAlumni",
			control: true,
			filter: false,
			wall: true,
			cache: false,
			max: "limit",
			limit: 4,
			iconPath: "img/dcsns/dcsns-dark/",
			imagePath: "img/dcsns/dcsns-dark/"
			});
		});
		</script>';
		}
		?>
<?php if ($pageNameClass == 'challenges'): ?>
			<!-- challenge page -->
		<link rel="stylesheet" href="css/thermometer-challenges.css?v=1">
<? endif;	?>
<?php if ($pageNameClass == 'impact'): ?>
		<!-- impact page -->
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/slick-theme.css?v=1">

<style>
	.cssanimations .suppoters--scroll ul{
		/*variable from the impact page.*/
		height: <?php echo $heightMobile ?>;
	}

	/* Move it (define the animation) */
		@-moz-keyframes scrollList {
		 0%   { -moz-transform: translateY(-<?php echo $heightMobile ?>); }
		 100% { -moz-transform: translateY(200px); }
		}
		@-webkit-keyframes scrollList {
		 0%   { -webkit-transform: translateY(-<?php echo $heightMobile ?>); }
		 100% { -webkit-transform: translateY(200px); }
		}
		@keyframes scrollList {
		 0%   { 
		 -moz-transform: translateY(-<?php echo $heightMobile ?>); /* Firefox bug fix */
		 -webkit-transform: translateY(-<?php echo $heightMobile ?>); /* Firefox bug fix */
		 transform: translateY(-<?php echo $heightMobile ?>);    
		 }
		 100% { 
		 -moz-transform: translateY(200px); /* Firefox bug fix */
		 -webkit-transform: translateY(200px); /* Firefox bug fix */
		 transform: translateY(200px); 
		 }
}


	
	@media(min-width:600px){
		.cssanimations .suppoters--scroll ul{
			/*variable from the impact page.*/
			height: <?php echo $heightDesktop ?>;
		}


	/* Move it (define the animation) */
		@-moz-keyframes scrollList {
		 0%   { -moz-transform: translateY(-<?php echo $heightDesktop ?>); }
		 100% { -moz-transform: translateY(200px); }
		}
		@-webkit-keyframes scrollList {
		 0%   { -webkit-transform: translateY(-<?php echo $heightDesktop ?>); }
		 100% { -webkit-transform: translateY(200px); }
		}
		@keyframes scrollList {
		 0%   { 
		 -moz-transform: translateY(-<?php echo $heightDesktop ?>); /* Firefox bug fix */
		 -webkit-transform: translateY(-<?php echo $heightDesktop ?>); /* Firefox bug fix */
		 transform: translateY(-<?php echo $heightDesktop ?>);    
		 }
		 100% { 
		 -moz-transform: translateY(200px); /* Firefox bug fix */
		 -webkit-transform: translateY(200px); /* Firefox bug fix */
		 transform: translateY(200px); 
		 }
		}
	}


</style>
<? endif; ?>

    </head>
    <body class="<?php echo $pageNameClass; ?>"> 
<div id="fb-root"></div>
<script>
$(document).ready(function() {
    $('.sharepost').click(function(e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    });
});</script>

	<!-- CountDown -->
    <section class="countdown">
		<div class="container">
			<div class="row">
				<div class="twelve column" style="position:relative;">
					<p>#NYUOneDay: <span id="countdown-holder"></span></p>
				</div>
			</div>
		</div>
	</section>
	<!-- /CountDown -->

	
		<header class="container clear">
			<div class="row">

				<div class="six columns logo clear">
					<a href="/"><img src="img/common/nyu-oneday--logo.png" style="vertical-align:top;" /></a>
					<a href="#" id="header-menu-btn" class="header-menu-btn "><i class="fa fa-bars"></i><span class="ln">menu</span></a>
				</div>

				<ul id="header-menu" class="six columns header-menu">
					<li class="menu-item menu-item--1"><a href="impact">Your Impact</a></li>
					<li class="menu-item menu-item--2"><a href="challenges">One Day Challenges</a></li>
					<li class="menu-item menu-item--3"><a href="givenow">Give Now</a></li>
				</ul>

			</div>
		</header>

