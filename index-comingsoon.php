
<?php
$title="NYU One Day";
$description="NYU One Day";
//$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
$INC_DIR = "inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Banner -->
        <!--<section class="banner-main">

			<div class="row">
				<div class="twelve column" style="position:relative;">
					<img src="img/banner.png" />
					<div class="overlay"><span class="banner-font">Get Ready for #NYUOneDay<br /><span class="banner-font-small">Coming 03.03.16</span></span></div>
				</div>
			</div>
		</section>-->
		<section class="banner clear">
<div class="container clear">
				<h1 class="banner-headline cs">
					<span class="ln ln1">One day. <br class="mobileBreak" />Countless dreams. </span> 
					<span class="ln ln2">Mark your calendar and join us. 3.23.17</span> <!-- <span class="ln">One NYU.</span> -->
				</h1>
		</div>
		</section>
		<!-- /Banner -->
		<!-- Content -->

		<!-- /Content -->
		<!-- Support today -->
<!-- 		<section class="supportToday">
			<div class="container clear">
				<p>On March 3rd – <strong>for one day</strong> – we’ll be asking the entire NYU community to join together for <strong>NYU Giving Day</strong>.  Join us for this unprecedented NYU event.</p>
				
			</div>
		</section> -->
		<!-- /Support today -->
		
		
		

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '798627976947401',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<?php require($INC_DIR. "footer.php"); ?>
