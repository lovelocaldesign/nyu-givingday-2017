
<?php
$title="NYU One Day";
$description="NYU One Day";
//$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
$INC_DIR = "inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Banner -->

		<section class="banner clear">
      <div class="container clear">
  				<h1 class="banner-headline">
<!--   					<span class="ln ln1">One day. <br class="mobileBreak" />Countless dreams. </span> 
  					 <span class="ln ln2">Mark your calendar <br>
             and join us. 3.23.17</span> -->
  				</h1>
  		</div>
		</section>
		<!-- /Banner -->


    <section class="presmessage clear clearfix">
      <div class="container clear">
        <div class="presmessage-col presmessage-video">
            <div class="videoOuterWrap">
              <div id="videoWrap" class="videoWrap">
                <iframe width="100%" height="" src="https://www.youtube-nocookie.com/embed/uSF5juK5NqQ" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
            <div class="block-videoLabel">
              <h2 class="video-name">Andrew Hamilton <br/>
              <span class="video-title">President</span></h2>
              
            </div>
        </div>
        <div class="presmessage-col presmessage-text">
          <h2 class="presmessage-headline ">
          <img src="img/hp-headline-pressmessage.png" alt="Section title01">
            <span class="sr-text">Section title01</span> 
          </h2>
           <p>
           Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eu placerat lorem. Curabitur tincidunt lectus vitae felis rutrum ultrices. In interdum tempor condimentum. Nullam semper finibus odio, ac dignissim diam interdum eget. Vivamus euismod tellus eu nunc vestibulum tempus. Nullam nunc odio, sagittis eget justo vel, accumsan volutpat lorem. Nunc ac ullamcorper nisi. Suspendisse dignissim, lectus in aliquet tincidunt, ex libero accumsan est, ut elementum dui quam quis ligula.
          </p>
           <a href="givenow.php" class="btn btn-green"> <span class="btn-inner">I will support #NYUONEDAY</span> </a>
        </div>

      </div>
    </section>
    <!-- /Banner -->


<section class="smSection">
  <div class="container clear"><a class="btn-link" href="">Check here for NYU One Day updates and breaking news.</a></div>
</section>


        <!-- donors  -->
    <section class="donors">
      <div class="container clear">
<!--         <div id="donors-thermometer" class="donors-thermometer donors-col donors-img"><img src="img/Chart-3-4-16.png"></div> -->
       <iframe id="donors-thermometer" class="donors-thermometer donors-col" src="inc/chart.html" frameborder="0" scrolling="no"></iframe>
        <div class="donors-text donors-col">
          <h2 class="donors-headline ">
            <img src="img/hp-headline-donor.png" alt="Section title01">
            <span class="sr-text">DREAMS ARE WAITING</span> 
          </h2>
          <p>
            They come from around the globe. Bright, bold, and eager &mdash; ready to make a difference in the world. <strong>They are the students of NYU.</strong> On NYU One Day, we call on our community to join together to support the dreams of every NYU student &mdash; dreams that are as diverse and varied as our university and as ambitious as our own. Real investments in real dreams, in real time. Ready? Let's do this.
          </p>
          <a href="givenow.php" class="btn"><span class="btn-inner">Join Us Now</span></a>
        </div>
      </div>
    </section>
    <!-- /donors -->



        <!-- social place -->
     <section class="social">
      <div class="container clear">
        <h2 class="social-headline">FOLLOW #NYUONEDAY</h2>
        <div id="social-stream"></div>
      </div>
    </section>
    <!-- /social -->

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '798627976947401',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<?php require($INC_DIR. "footer.php"); ?>
