$(document).ready(function() {
    $('#Show').hide();
    $("input[name=different-billing]").click(function () {
        $('#Show').toggle('slow');
    });

    //Mobile menu
  var menuBtn = $('#header-menu-btn');
  var menuContainer = $('#header-menu');
	 $( menuBtn ).click(function (event) {

	 	event.preventDefault(event);
        $(menuContainer).slideToggle();
        return false;
    });
 });