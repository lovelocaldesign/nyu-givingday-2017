//<![CDATA[
    // nyu.js

    $(function() {

       $("body").on("submit", '#givenowform', function(e) {

            e.preventDefault();

            $('#errmsg').html('Processing ... ');
            $('#submitbutton').html('');

            $.ajax({
                url: 'givenowresponse.php',
                dataType : 'json',
                type: 'POST',
                data: {
                    action: 'givenowformsubmit',
                    frmdata: $(this).serialize()
                },
                success: function(response) {
                    $('#'+response['dv']).html(response['msg']);
                    if ('errmsg' == response['dv']) {
                       $('html, body').animate({
                          scrollTop: $('#errmsg').offset().top
                       }, 3000);
                       $('#submitbutton').html('<input type="submit" value="Give Now" placeholder="Give Now" />');
                    } else {
                       $('html, body').animate({
                          scrollTop: 0
                       }, 3000);

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
                }
            });

        });


       $("body").on("change", '#country', function(e) {

         if ('USA' == $('#country').val()) {

            $('#zipdiv').html('<input type="text" name="zip" placeholder="Zip*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'usacountry'},
               success: function(response) {
                  $('#statediv').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })

         } else if ('UMI' == $('#country').val()) {

            $('#zipdiv').html('<input type="text" name="zip" placeholder="Postal Code*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'umicountry'},
               success: function(response) {
                  $('#statediv').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })


         } else if ('CAN' == $('#country').val()) {

            $('#zipdiv').html('<input type="text" name="zip" placeholder="Postal Code*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'cancountry'},
               success: function(response) {
                  $('#statediv').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })

         } else {

            $('#statediv').html('<input type="text" name="state" placeholder="State" />');
            $('#zipdiv').html('<input type="text" name="zip" placeholder="Postal Code" />');

         }

       });


       $("body").on("change", '#make-gift', function(e) {

          // console.log($(this).val());

          $('#campaign').html('');

          $.ajax({
             url: 'givenowresponse.php',
             dataType : 'json',
             type: 'POST',
             data: { action: 'fundcategory', cat: $(this).val() },
             success: function(response) {
               $('#subcat').html(response['msg']);
             },
             error: function(jqXHR, textStatus, errorThrown) {
               alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
             }
          })

       });


       $("body").on("change", '#campaign-name', function(e) {

          //console.log($(this).val());
          //console.log($('#make-gift').val());

          $.ajax({
             url: 'givenowresponse.php',
             dataType : 'json',
             type: 'POST',
             data: { action: 'campaignname', cat: $('#make-gift').val(), cam: $(this).val() },
             success: function(response) {
               $('#campaign').html(response['msg']);
             },
             error: function(jqXHR, textStatus, errorThrown) {
               alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
             }
          })

       });


       $("body").on("change", '#billing-country', function(e) {

         if ('USA' == $('#billing-country').val()) {

            $('#billzip').html('<input type="text" name="billing-zip" placeholder="Billing Zip*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'usabillcountry'},
               success: function(response) {
                  $('#billstate').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })

         } else if ('UMI' == $('#billing-country').val()) {

            $('#billzip').html('<input type="text" name="billing-zip" placeholder="Billing Postal Code*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'umibillcountry'},
               success: function(response) {
                  $('#billstate').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })


         } else if ('CAN' == $('#billing-country').val()) {

            $('#billzip').html('<input type="text" name="billing-zip" placeholder="Billing Postal Code*" />');

            $.ajax({
               url: 'givenowresponse.php',
               dataType : 'json',
               type: 'POST',
               data: { action: 'canbillcountry'},
               success: function(response) {
                  $('#billstate').html(response['msg']);
               },
               error: function(jqXHR, textStatus, errorThrown) {
                  alert('jqXHR: '+jqXHR+'\ntextStatus: '+textStatus+'\nerrorThrown: '+errorThrown);
               }
            })

         } else {

            $('#billstate').html('<input type="text" name="billing-state" placeholder="Billing State" />');
            $('#billzip').html('<input type="text" name="billing-zip" placeholder="Billing Postal Code" />');

         }

       });

    }); // $(function()

//]]>

