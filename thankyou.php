<?php
if (strlen(session_id()) < 1) {
    session_start();
}
# disabled for testing !!!!!
#if (!isset($_REQUEST['auth_trans_ref_no'])) {
#   header("location:givenow");
#   exit();
#}

require_once "data.class.php";
$dt = new DT;

$queryArr = $dataArr = $tmpArr = array();
foreach ($_REQUEST as $k => $v) {
    $queryArr[$k] = isset($v) && $v ? urldecode(trim(strip_tags($v))) : '';
    # $dt->debugLog("{$k} = {$v}");
} # foreach

$ence = isset($_REQUEST['enc']) && $_REQUEST['enc'] ? trim(strip_tags($_REQUEST["enc"])) : (isset($_SESSION['enc']) && $_SESSION['enc'] ? trim(strip_tags($_SESSION["enc"])) : '');

if (isset($ence) && $ence) {
   $_SESSION['enc'] = $ence;
}

require_once "db.class.php";
$db = new DB;

$queryArr['campname'] = $dt->getFundName($queryArr['merchantDefinedData2']);
$queryArr['campname'] = isset($queryArr['campname']) && $queryArr['campname'] ? $queryArr['campname'] : "NYUOneDay";

$tmpArr = explode('-', $queryArr['req_card_expiry_date']);

$dataArr['anonymous'] = isset($_SESSION["anonymous"]) && $_SESSION["anonymous"] ? 1 : 0;
$dataArr['first-name'] = isset($queryArr['FIRST_NAME']) && $queryArr['FIRST_NAME'] ? $queryArr['FIRST_NAME'] : '';
$dataArr['middle-name'] =  isset($_SESSION['mname']) && $_SESSION['mname'] ? $_SESSION['mname'] : '';
$dataArr['last-name'] = isset($queryArr['LAST_NAME']) && $queryArr['LAST_NAME'] ? $queryArr['LAST_NAME'] : '';
$dataArr['street-address'] = isset($queryArr['ADDRESS_LINE_1']) && $queryArr['ADDRESS_LINE_1'] ? $queryArr['ADDRESS_LINE_1'] : '';
$dataArr['city'] = isset($queryArr['CITY']) && $queryArr['CITY'] ? $queryArr['CITY'] : '';
$dataArr['state'] = isset($queryArr['STATE']) && $queryArr['STATE'] ? $queryArr['STATE'] : '';
$dataArr['zip'] = isset($queryArr['POSTAL_CODE']) && $queryArr['POSTAL_CODE'] ? $queryArr['POSTAL_CODE'] : '';
$dataArr['country'] = isset($queryArr['COUNTRY']) && $queryArr['COUNTRY'] ? $queryArr['COUNTRY'] : '';
$dataArr['phone'] = isset($queryArr['PHONE']) && $queryArr['PHONE'] ? $queryArr['PHONE'] : '';
$dataArr['email'] = isset($queryArr['EMAIL']) && $queryArr['EMAIL'] ? $queryArr['EMAIL'] : '';
$dataArr['billing-street-address'] = isset($queryArr['billTo_street1']) && $queryArr['billTo_street1'] ? $queryArr['billTo_street1'] : '';
$dataArr['billing-city'] = isset($queryArr['billTo_city']) && $queryArr['billTo_city'] ? $queryArr['billTo_city'] : '';
$dataArr['billing-state'] = isset($queryArr['billTo_state']) && $queryArr['billTo_state'] ? $queryArr['billTo_state'] : '';
$dataArr['billing-zip'] = isset($queryArr['billTo_postalCode']) && $queryArr['billTo_postalCode'] ? $queryArr['billTo_postalCode'] : '';
$dataArr['billing-country'] = isset($queryArr['billTo_country']) && $queryArr['billTo_country'] ? $queryArr['billTo_country'] : '';
$dataArr['net-id'] = isset($queryArr['OTHER_ID']) && $queryArr['OTHER_ID'] ? $queryArr['OTHER_ID'] : '';
$dataArr['card-name'] = '';
$dataArr['card-number'] = isset($queryArr['req_card_number']) && $queryArr['req_card_number'] ? substr($queryArr['req_card_number'],-4,4) : '';
$dataArr['cvc'] = isset($queryArr['auth_avs_code_raw']) && $queryArr['auth_avs_code_raw'] ? $queryArr['auth_avs_code_raw'] : '';
$dataArr['gift'] = isset($queryArr['orderAmount']) && $queryArr['orderAmount'] ? $queryArr['orderAmount'] : '';
$dataArr['allocation'] = isset($queryArr['merchantDefinedData2']) && $queryArr['merchantDefinedData2'] ? $queryArr['merchantDefinedData2'] : '';
$dataArr['transidstr'] = isset($queryArr['transaction_id']) && $queryArr['transaction_id'] ? $queryArr['transaction_id'] : '';
$dataArr['authcode'] = isset($queryArr['auth_trans_ref_no']) && $queryArr['auth_trans_ref_no'] ? $queryArr['auth_trans_ref_no'] : '';
$dataArr['transid'] = isset($queryArr['requestID']) && $queryArr['requestID'] ? $queryArr['requestID'] : '';
$dataArr['expiration-month'] = isset($tmpArr[0]) && $tmpArr[0] ? $tmpArr[0] : '';
$dataArr['expiration-year'] = isset($tmpArr[1]) && $tmpArr[1] ? $tmpArr[1] : '';
$dataArr['make-gift'] = isset($_SESSION['make-gift']) && $_SESSION['make-gift'] ? $_SESSION['make-gift'] : '';
$dataArr['campaign-name'] = isset($_SESSION['campaign-name']) && $_SESSION['campaign-name'] ? $_SESSION['campaign-name'] : $queryArr['campname'];
$dataArr['fund-name'] = isset($_SESSION['fund-name']) && $_SESSION['fund-name'] ? $_SESSION['fund-name'] : '';
$dataArr['other-fund'] = isset($_SESSION['other-fund']) && $_SESSION['other-fund'] ? $_SESSION['other-fund'] : '';

$db->recordDonation($dataArr);
$dt->sendgift2MC($dataArr);

$title="NYU One Day: Give Now";
$description="NYU One Day: Give Now";
$INC_DIR = $_SERVER["DOCUMENT_ROOT"]. "/inc/";
//Empty variables should be declared NULL (without quotes) like: $title=NULL;
require($INC_DIR. "header.php"); ?>
<!-- code all <body> tag the content here -->

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="container" id="container">
        <!-- Give Now Thank You -->
        <section class="thankyou-msg">
			<div class="row">
				<div class="twelve columns thank-you-msg"><?php // not used echo $queryArr['campname']; ?>
                   <h1 class="banner-thankyou-headline">Thank You!</h1>
				   <p class="copy copy--center"><?php echo $queryArr['FIRST_NAME']; ?>, thank you for your gift of $<?php echo $queryArr['AMOUNT_GFT_1']; ?> to NYU One Day. <br/>
           Your confirmation number is <?php echo $queryArr['transaction_id']; ?>. </p>
                   <h3><span class="ln">This is NYU.</span> <span class="ln">This is how we do dreams.</span></h3>
                   
                   <p class="copy copy--center">
                   There is still time for others to join us. Please share #NYUOneDay and tell your friends how you did giving on NYU One Day! #NYUOneDay #VioletPride</p>

               <ul class=" socialicon--thankyou">
                  <li><a class="sharepost" href="https://www.facebook.com/sharer/sharer.php?u=nyuoneday.org" target="_blank"><i class="fa fa-facebook"></i></a></li>
                  <li><a class="sharepost" href="https://twitter.com/intent/tweet/?text=I%20joined%20%23NYUOneDay.%20Will%20you%3F&url=https%3A%2F%2Fnyuoneday.org%2F"
   target="_blank"><i class="fa fa-twitter"></i></a></li>
               </ul>
				</div>
			</div>
		</section>
		<!-- /Give Now Thank You -->

	</div>
<?php require($INC_DIR. "footer.php"); ?>

<?php

   $_SESSION = array();

   if (isset($_COOKIE["enc"])) {
      setcookie("enc", "", time()-42000, "/");
   }

   session_destroy();

?>
